/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simpleclient;

/**
 *
 * @author pfb1
 */
public class NotConnectedException extends Exception
{
    public NotConnectedException()
    {
        super("Socket not connected");
    }
    
    public NotConnectedException(String message)
    {
        super(message);
    }
}
